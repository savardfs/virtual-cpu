/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terminal;

import java.util.ArrayList;

/**
 *
 * @author fanma
 */
public class IAR {
    private ArrayList<String> buffer = new ArrayList<String>();
    private ArrayList<String> instructionType = new ArrayList<String>();
    private boolean ready = false;
    private ArrayList<Boolean> freeze = new ArrayList<Boolean>();
    
    //add new element to buffer
    private void addInstruction(String i){
        buffer.add(i);
    }
    
    //add new element to type
    private void addType(String t){
        instructionType.add(t);
    }
    
    private void addFreeze(boolean f){
        freeze.add(f);
    }
    
    public ArrayList getInstruction(){
        ArrayList instructions = new ArrayList();
        instructions.add(buffer.get(0));
        buffer.remove(0);
        instructions.add(instructionType.get(0));
        instructionType.remove(0);
        instructions.add(freeze.get(0));
        freeze.remove(0);
        
        if(buffer.toArray().length == 0){
            ready=false;
        }
        
        return instructions;
    }
    
    public boolean isReady(){
        return ready;
    }
    
    public void giveInstruction(String i, String t){
        addInstruction(i);
        addType(t);
        addFreeze(false);
        ready = true;
    }
    
    public void giveInstruction(String i, String t, boolean f){
        addInstruction(i);
        addType(t);
        addFreeze(f);
        ready = true;
    }
}
