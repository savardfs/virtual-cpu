/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terminal;

import java.util.ArrayList;

/**
 *
 * @author fanma
 */
public class CPU extends Thread{
    
    //16 registers available to CPU
    //registers can be frozen at will
    private Register[] r = new Register[16];
    private boolean[] frozen = new boolean[r.length];
    private IAR iar = new IAR();
    
    public CPU(){
        for(int i=0;i<r.length;i++){
            r[i] = new Register();
            frozen[i]=false;
        }
    }
    
    public void registerOperation(int firstRegister, int secondRegister, int storedRegister, String instructionType){
        r[storedRegister] = r[firstRegister].getNoValueChange(r[secondRegister], instructionType);
        
    }
    
    //give exact register you want to use
    public void giveInstruction(String instruction, int register, String instructionType){
        r[register].giveInstruction(instruction, instructionType);
    }
    
    //freeze register from use automatically by system
    public void freeze(int r){
        frozen[r] = true;
    }
    
    //unfreeze register
    public void unfreeze(int r){
        frozen[r] = false;
    }
    
    //system crawls through and freezes all non-0 values from poke
    public void freeze(){
        for(int i=0; i<r.length;i++){
            if(!r[i].poke().equals("00000000")){
                frozen[i]=true;
            }
        }
    }
    
    //system crawls through and unfreezes all non-0 values
    public void unfreeze(){
        for(int i=0; i<r.length;i++){
            if(!r[i].poke().equals("00000000")){
                frozen[i]=false;
            }
        }
    }
    
    //system decides what register to use
    //limitation: will leave at least 2 registers free (r14 and r15 under base system)
    //limitation: cpu does not freeze automatically unless prompted
    public void giveInstruction(String instruction, String instructionType){
        boolean lastRegisters = true;
        for(int i=0;i<r.length-2;i++){
            if(!frozen[i]){
                r[i].giveInstruction(instruction, instructionType);
                //r[i].poke();
                lastRegisters=false;
                break;
            }
        }
        
        //prefer registers that don't have values
        //otherwise, take last register as a last resort
        if(lastRegisters){
            if(r[r.length-2].poke().equals("00000000")){
                r[r.length-2].giveInstruction(instruction, instructionType);
                //r[r.length-2].poke();
            }
            else{
                r[r.length-1].giveInstruction(instruction, instructionType);
                //r[r.length-1].poke();
            }
        }
    }
    
    //system decides what register to use
    //limitation: will leave at least 2 registers free (r14 and r15 under base system)
    //limitation: cpu will only freeze when told to freeze
    //no auto freezing based on probability
    public void giveInstruction(String instruction, String instructionType, boolean freeze){
        boolean lastRegisters = true;
        for(int i=0;i<r.length-2;i++){
            if(!frozen[i]){
                r[i].giveInstruction(instruction, instructionType);
                if(freeze){
                    frozen[i]=true;
                }
                lastRegisters=false;
                break;
            }
        }
        
        //prefer registers that don't have values
        //otherwise, take last register as a last resort
        if(lastRegisters){
            if(r[r.length-2].poke().equals("00000000")){
                r[r.length-2].giveInstruction(instruction, instructionType);
                //r[r.length-2].poke();
            }
            else{
                r[r.length-1].giveInstruction(instruction, instructionType);
                //r[r.length-1].poke();
            }
        }
    }
    
    public void feedIAR(String i, String t){
        iar.giveInstruction(i, t);
    }
    
    //for when you want to freeze the register
    public void feedIAR(String i, String t, boolean f){
        iar.giveInstruction(i, t, f);
    }
    
    //next we have the trolling function for the CPU
    //this will pull instructions from the IAR for as long as they exist
    //and feed them automatically to the registers.
    //supports freezing through boolean values.
    public void checkIAR(){
        while(iar.isReady()){
            ArrayList instructions = iar.getInstruction();
            this.giveInstruction((String)instructions.get(0), (String)instructions.get(1), (boolean)instructions.get(2));
        }
    }
    
    
    
    public void dumpAll(){
        for(int i=0;i<r.length;i++){
            System.out.println(r[i].poke());
        }
    }

    @Override
    public void run() {
        try{
            
            while(true){
            checkIAR();
            Thread.sleep(10);
            }
        }catch(Exception e){
            System.out.println("Probably shouldn't see this.");
        }
    }
    
    

    
}
