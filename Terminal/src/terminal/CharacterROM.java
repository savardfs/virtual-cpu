/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terminal;

/**
 *
 * @author fanma
 */
public class CharacterROM {
    
    public boolean[][] pixels;
    
    private boolean[][] defineChars(String instruction){
        boolean[][] chars = new boolean[0][0];
        switch(instruction){
            case "00000000"://space
                chars = new boolean[][]
                {   {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                    {false, false, false, false},
                };
                break;
            case "01000001"://A
                chars = new boolean[][]
                {   {true, true, true, true},
                    {true, false, false, true},
                    {true, false, false, true},
                    {true, true, true, true},
                    {true, true, true, true},
                    {true, false, false, true},
                    {true, false, false, true},
                    {true, false, false, true},
                };
                
        }
        
        return chars;
        
    }
    
    public void testPrint(){
        pixels = defineChars("01000001");
        
        for(int i=0;i<pixels.length; i++){
            for(int j=0;j<pixels[i].length; j++){
                if(pixels[i][j]){
                    System.out.print("1");
                }
                else{
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }
    
}
