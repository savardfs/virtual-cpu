/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terminal;

/**
 *
 * @author fanma
 */
public class VideoChip {
    
    /*
    The purpose of the video chip is to blast character data to the screen.
    Individual pixels *can* be manipulated, but that's a graphics mode that
    isn't the primary one.
    
    The Video Chip accepts an instruction, checks against a character ROM for that
    character, and pushes it to the screen.
    
    Parameters that need to be defined in the instruction:
    -area on screen (x,y)
    -size of element if not char data
    
    Parameters that need to be kept track of:
    -chars already on screen
    */
    
    private boolean[][] screen = new boolean[320][200];
    
    public VideoChip(){
        for(int i = 0; i<screen.length; i++){
            for(int j=0;j<screen[i].length;j++){
                screen[i][j] = false;
            }
        }
    }
    
    public void setPixel(int x, int y){
        screen[x][y] = true;
    }
    
    public void clearPixel(int x, int y){
        screen[x][y] = false;
    }
    
    public boolean[][] dumpScreen(){
        return screen;
    }
    
}
