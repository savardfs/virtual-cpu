/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terminal;

/**
 *
 * @author fanma
 */
public class Register {
    /*
    The basic idea is to create a working CPU within Java that you can write instructions to.
    From other courses on how computers work, I know to start with registers.
    Each register will have 8 possible bits (thus making this an 8-bit computer)
    and will respond to different cpu commands.
    
    The CPU will have 16 registers labeled 0-15.
    
    I will then write commands that will tell the registers to do specific things.
    Output will br brought to the terminal to begin, but I plan to add a string mode
    that will print to the screen too.
    
    
    */
    
    boolean[] bits = new boolean[8];
    
    
    public enum INSTRUCTION{CLEAR,ADD,SUBTRACT,STORE,MIN,MAX}
    
    public Register(){
        for(int i=0;i<bits.length;i++){
            bits[i] = false;
        }
    }
    
    /**
    @param instruction The binary string given to the register.
    * @param i The type of instruction given to the register.
    */
    public void giveInstruction(String instruction, INSTRUCTION i){
        //string MUST be reversed for everything to work
        instruction = reverse(instruction);
        //System.out.println("instruction: " + instruction);
        switch(i){
            case ADD:
                add(instruction);
                break;
            case SUBTRACT:
                subtract(instruction);
                break;
            case STORE:
                store(instruction);
                break;
            case MIN:
                store("00000000");
                break;
            case MAX:
                store("11111111");
                break;
            default:
                poke();
            
        }
    }
    
    public void giveInstruction(String instruction, String instructionType){
        INSTRUCTION inst = INSTRUCTION.CLEAR;
        switch(instructionType){
            case "ADD":
                inst=INSTRUCTION.ADD;
                break;
            case "SUBTRACT":
                inst=INSTRUCTION.SUBTRACT;
                break;
            case "CLEAR":
                inst=INSTRUCTION.CLEAR;
                break;
            case "MIN":
                inst=INSTRUCTION.MIN;
                break;
            case "MAX":
                inst=INSTRUCTION.MAX;
                break;
            case "STORE":
                inst=INSTRUCTION.STORE;
                break;
            default:
                inst=INSTRUCTION.CLEAR;
            
        }
        giveInstruction(instruction, inst);
    }
    
    private void store(String instruction){
        //instruction = reverse(instruction);
        int i=0;
        for(char c:instruction.toCharArray()){
            if(c=='1'){
                bits[i] = true;
            }
            else{
                bits[i] = false;
            }
            i++;
        }
    }
    
    private String reverse(String instruction){
        
        //preparing for 2's complement arithmetic
        instruction = new StringBuilder(instruction).reverse().toString();
        
        return instruction;
    }
    
    public String poke(){
        //remember to reverse the string coming back when poking
        //System.out.println(reverse(toString(bits)));
        return reverse(toString(bits));
    }
    
    //to add one when subtracting
    private boolean[] addOne(boolean[] bits){
        boolean[] inst = new boolean[bits.length];
        
        for(int i=0;i<inst.length;i++){
            inst[i]=false;
            if(i==0){
                inst[i]=true;
            }
        }
        
       
        
        boolean carry = false;
        
        
        
        for(int j=0;j<inst.length;j++){
            if(carry){
                if(bits[j]){
                    bits[j] = false;
                    carry=true;
                }
                else{
                    bits[j]=true;
                    carry=false;
                }
            }
            
            if(inst[j]){
                if(bits[j]){
                    bits[j]=false;
                    carry=true;
                }
                else{
                    bits[j]=true;
                }
     
            }
            
            
        }
            
        
        //System.out.println(toString(bits));
        return bits;
    }
    
    //add one instruction to bits
    private void add(String instruction){
        boolean[] inst = new boolean[bits.length];
        int i=0;
        for(char c:instruction.toCharArray()){
            if(c=='1'){
                inst[i] = true;
            }
            else{
                inst[i] = false;
            }
            i++;
        }
        
        boolean carry = false;
        
        for(int j=0;j<inst.length;j++){
            if(carry){
                if(bits[j]){
                    bits[j] = false;
                    carry=true;
                }
                else{
                    bits[j]=true;
                    carry=false;
                }
            }
            
            if(inst[j]){
                if(bits[j]){
                    bits[j]=false;
                    carry=true;
                }
                else{
                    bits[j]=true;
                }
     
            }
            
            
        }
    }
    
    //subtract by 2s complement
    private void subtract(String instruction){
        //instruction = reverse(instruction);
        boolean[] inst = new boolean[bits.length];
        int i=0;
        for(char c:instruction.toCharArray()){
            if(c=='1'){
                inst[i] = true;
            }
            else{
                inst[i] = false;
            }
            i++;
        }
        
       
        
        i=0;
        
        for(boolean b:inst){
            if(b){
                inst[i]=false;
            }
            else{
                inst[i]=true;
            }
            i++;
        }
        
        i=0;
        
        //add one to complete the 2s complement
        inst = addOne(inst);
        //System.out.println("inst here is: " + toString(inst));
        add(toString(inst));
        
        
    }
    
    
    public String toString(boolean[] instruction){
        String returnVal = "";
        for(int j = 0;j<instruction.length;j++){
            String c;
            
            if(instruction[j]){
                c = "1";
            }
            else{
                c="0";
            }
            returnVal+=c;
            
        }
        
        return returnVal;
    }
    
    public void combine(Register r, String i){
        String instruction = reverse(r.poke());
        INSTRUCTION inst = INSTRUCTION.CLEAR;
        
        switch(i){
            case "ADD":
                inst=INSTRUCTION.ADD;
                break;
            case "SUBTRACT":
                inst=INSTRUCTION.SUBTRACT;
                break;
            case "CLEAR":
                inst=INSTRUCTION.CLEAR;
                break;
            case "MIN":
                inst=INSTRUCTION.MIN;
                break;
            case "MAX":
                inst=INSTRUCTION.MAX;
                break;
            case "STORE":
                inst=INSTRUCTION.STORE;
                break;
            default:
                inst=INSTRUCTION.CLEAR;
            
        }
        
        giveInstruction(instruction, inst);
    }
    
    public Register getNoValueChange(Register r, String t){
        String first = this.poke();
        //System.out.println(first + " is first");
        String second = r.poke();
        
        Register x = new Register();
        //Had to make sure to only go through the instruction method
        //Otherwise, the string would NEVER reverse.
        x.giveInstruction(first,"STORE");
        
        x.giveInstruction(second, t);
        
        return x;
    }
}
